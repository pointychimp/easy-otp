#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <openssl/sha.h>
#include <getopt.h> // for parsing command line options
#include <unistd.h> // for blocking input as password is typed: getpass()
#include <algorithm> // for std::transform only at the moment
#include <cstring> // for strcmp() only at the moment
/* http://libslack.org/manpages/getopt.3.html
   http://www.gnu.org/software/libc/manual/html_node/Example-of-Getopt.html
   http://www.gnu.org/software/libc/manual/html_node/Getopt-Long-Option-Example.html */

const std::string dataFileName = "./ezotp.dat";

bool simpleSHA256(void* input, unsigned long length, unsigned char* md)
{
    SHA256_CTX context;
    if(!SHA256_Init(&context))
        return false;

    if(!SHA256_Update(&context, (unsigned char*)input, length))
        return false;

    if(!SHA256_Final(md, &context))
        return false;

    return true;
}

std::string getHexRepresentation(const unsigned char* bytes, size_t length)
{
    std::ostringstream os;
    os.fill('0');
    os << std::hex;
    for (const unsigned char * ptr = bytes; ptr < bytes + length; ptr++)
    {
        os << std::setw(2) << (unsigned int)*ptr;
    }
    return os.str();
}

void firstRun()
{
    char *password;
    std::string newAccountName;
    std::string happyWithName;
    char seed[256];
    std::string seedStr;
    unsigned char hash[SHA_DIGEST_LENGTH]; // 20

    std::cout << "This is your first run!\n";
    std::cout << "Creating data file \"ezotp.dat\" ...\n";
    std::cout << "------------\n";
    std::cout << "You should encrypt this file with a password to keep it safer.\n";
    std::cout << "Enter password here, or hit enter to go without one (DANGEROUS): ";
    password = getpass("");
    std::cout << "------------\n";
    do
    {
        std::cout << "What would you like the account to be called?\n";
        std::cout << "Spaces have to be escaped with \\ when using the program later" << std::endl;
        getline(std::cin, newAccountName);

        std::cout << "Is \"" << newAccountName << "\" correct? (Y/n) ";
        getline(std::cin, happyWithName);
        std::transform(happyWithName.begin(), happyWithName.end(), happyWithName.begin(), ::tolower);
    } while (happyWithName != "yes" && happyWithName != "y" && happyWithName != "");
    std::cout << "------------\n";
    std::cout << "Mash alphanumeric keys for around 15 seconds and press enter.\n";
    std::cout << "This will be the seed that generate this account's OTPs.\n";
    std::cout << "The more random the better!\n";
    std::cin.getline(seed, 256);

    //SHA1((const unsigned char*)seed, sizeof(seed) - 1, hash);

    unsigned char md[SHA256_DIGEST_LENGTH]; // 32 bytes
    if(!simpleSHA256(seed, sizeof(seed), md))
    {
        std::cout << "err\n";
        // handle error
    }
    else
    {
        std::cout << getHexRepresentation(md, sizeof(md)) << std::endl;
    }

    /*if (strcmp(password, "") == 0)
    {
        std::cout << "no password\n";
    }
    else
    {
        std::cout << "\"" << password << "\"\n";
    }*/

}
bool usageDisplayed = false;
void usage()
{
    if (usageDisplayed) return;
    std::cout << "Usage:\n";
    std::cout << "--help -h ............. This help\n";
    std::cout << "--name -n <name> ...... Create new profile with name <name>\n";
    usageDisplayed = true;
}

int main(int argc, char *argv[])
{
    // these two set if --name <name> is passed
    // will be name for new account
    bool makingNewAccount = false;
    std::string newAccountName;
    // contains the short version of each option passed
    // so it can be switched
    char c;
    // contains index so if long option details need to be
    // referenced, they can be
    int longOptIndex;
    // actual long options and their short representations
    // see http://www.gnu.org/software/libc/manual/html_node/Getopt-Long-Option-Example.html
    struct option long_options[] =
    {
        {"help", no_argument, 0, 'h'},
        {"name", required_argument, 0, 'n'}
    };

    // only one arg (the program name)
    if (argc == 1)
    {
        std::fstream dataFile(dataFileName.c_str(), std::ios::in);
        if (dataFile.good()) // data file exists, remind user how to use program
        {
            usage();
        }
        else
        {
            // close the file since opening it for input doesn't create a
            // non-existent file. Then open it for output since that creates it
            dataFile.close();
            ///dataFile.open(dataFileName.c_str(), std::ios::out);
            firstRun(); // display helpful information if this was first run
        }
        dataFile.close();
    }
    else
    {
        // get each short arg representation out of argv
        while ((c = getopt_long(argc, argv, "hn:", long_options, &longOptIndex)) != -1)
        {
            // switch the current option
            switch (c)
            {
                case 'h': // help
                    usage();
                    break;
                case 'n': // new account
                    makingNewAccount = true;
                    newAccountName = optarg;
                    break;
                case '?': // option not recognized or no argument if required
                    if (optopt == 'n')
                        std::cout << "Need to specify a new name\n";
                    else if (isprint(optopt))
                        {} // unknown option
                    else
                        usage(); // really don't know what happened ???
                    break;
                default:
                    {
                        // not sure if this is ever run to be honest
                        usage();
                        return 1;
                    }
            }; // switch (c)
        } // while (all the options)
    } // else (argc > 1)

    if (makingNewAccount)
    {
        std::cout << "New account name: " << newAccountName << "\n";
    }

    return 0;
}
